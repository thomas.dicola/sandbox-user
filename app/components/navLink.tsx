import Link from "next/link";
import { usePathname } from "next/navigation";

interface NavLinkProps {
  href: string;
  label: string;
}

export default function NavLink({ href, label }: NavLinkProps) {
  const pathanme = usePathname();
  const isActive = pathanme === href;
  return (
    <li>
      <Link
        href={href}
        className={isActive ? "text-blue-600" : ""}
        aria-current="page"
      >
        {label}
      </Link>
    </li>
  );
}
