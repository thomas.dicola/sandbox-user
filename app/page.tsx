import { Alert } from "flowbite-react";
import Card from "./components/card";
import Nav from "./components/nav";
import Hero from "./components/hero";
import { Button } from "@/components/ui/button";

export default function Home() {
  return (
    <div className="my-auto h-screen">
      <Hero />
    </div>
  );
}
