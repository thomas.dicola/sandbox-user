import Card from "../components/card";
import watch from "../assets/watch.jpeg";
import mac from "../assets/mac.png";
import mini from "../assets/mini.jpeg";
import OrderSummary from "../components/orderSummary";
import CheckoutPage from "../components/checkoutPage";
import Payment from "../components/payment";

export default function Shop() {
  return (
    <>
      <div className="mx-auto flex flex-wrap justify-around">
        <Card
          description="Apple Watch (Series SE) 2022 GPS 40 mm - Aluminium Minuit - Bracelet sport Noir"
          pictureSrc={watch}
          price={279.99}
        />
        <Card
          description="Apple MacBook Air 13,3 M1 8Go 256Go Gris Sidéral"
          pictureSrc={mac}
          price={931.0}
        />
        <Card
          description="Mac mini (Novembre 2020) M1 3,2 GHz - SSD 256 Go - 8Go"
          pictureSrc={mini}
          price={501.0}
        />
      </div>
      <CheckoutPage />
      <OrderSummary />
      <Payment />
    </>
  );
}
